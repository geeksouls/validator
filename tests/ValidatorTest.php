<?php
/**
 * ValidatorTest File
 *
 * PHP Version 5.6
 *
 * @category Class
 * @package  Validators
 * @author   Jerin Kuriakose <jerintk8@gmail.com>
 * @license  MIT License
 * @link     https://packagist.org/packages/jthedev/validators
 */

use PHPUnit\Framework\TestCase;
use Jthedev\Validators\Validator;
use Jthedev\Validators\Exceptions\ValidationException;

/**
 * ValidatorTest Class
 *
 * PHP Version 5.6
 *
 * @category Class
 * @package  Validators
 * @author   Jerin Kuriakose <jerintk8@gmail.com>
 * @license  MIT License
 * @link     https://packagist.org/packages/jthedev/validators
 */
class ValidatorTest extends TestCase
{
    /**
     * Test the instance creation
     *
     * @return true
     */
    public function testInstanceCheck()
    {
        $validator = new Validator();
        $this->assertInstanceOf(Validator::class, $validator);
    }

    /**
     * Test the validator for true with the values in POST
     *
     * @param array $params Array of test cases
     *
     * @dataProvider paramsWithoutValue
     *
     * @return assertions
     */
    public function testForTruePost($params)
    {
        $_POST['email'] = 'jamie@domain.com';
        $_POST['password'] = 'Petlover_785';
        $_POST['phone'] = 12345;

        $validator = new Validator();
        $this->assertTrue($validator->validate($params));
    }

    /**
     * Provide default parameters without value
     *
     * @return true
     */
    public function paramsWithoutValue()
    {
        return [
                    [
                        [
                            'email' => [
                                            'required' => true,
                                            'type' => 'email',
                                            'minlen' => 5,
                                            'maxlen' => 20
                                            ],
                            'password' => [
                                            'required' => true,
                                            'type' => 'password',
                                            'minlen' => 5
                                            ],
                            'phone' => [
                                        'type' => 'integer',
                                        'minlen' => 5,
                                        'maxlen' => 7
                                        ]
                        ]
                    ],
                    [
                        [
                            'email' => [
                                            'required' => false,
                                            'type' => 'email',
                                            'minlen' => 5,
                                            'maxlen' => 20
                                            ],
                            'password' => [
                                            'required' => false,
                                            'type' => 'password',
                                            'minlen' => 5
                                            ],
                            'phone' => [
                                        'required' => true,
                                        'type' => 'integer',
                                        'minlen' => 5,
                                        'maxlen' => 7
                                        ]
                        ]
                    ]
                ];
    }

    /**
     * Test the validator for true with the given parameters
     *
     * @param array $params Array of test cases
     *
     * @dataProvider paramsWithValue
     * @return       assertions
     */
    public function testForTrueParam($params)
    {
        $validator = new Validator();
        $this->assertTrue($validator->validate($params));
    }

    /**
     * Provide default parameters with value
     *
     * @return true
     */
    public function paramsWithValue()
    {
        return [
                    [
                        [
                            'email' => [
                                            'required' => true,
                                            'type' => 'email',
                                            'minlen' => 5,
                                            'maxlen' => 20,
                                            'value' => 'claire@xyz.com'
                                            ],
                            'password' => [
                                            'required' => true,
                                            'type' => 'password',
                                            'minlen' => 5,
                                            'value' => '123456'
                                            ],
                            'phone' => [
                                        'type' => 'integer',
                                        'minlen' => 5,
                                        'maxlen' => 7,
                                        'value' => 234234
                                        ]
                        ]
                    ],
                    [
                        [
                            'email' => [
                                            'required' => false,
                                            'type' => 'email',
                                            'minlen' => 5,
                                            'maxlen' => 20,
                                            'value' => 'jamie@xyz.com'
                                            ],
                            'password' => [
                                            'required' => false,
                                            'type' => 'password',
                                            'minlen' => 5,
                                            'value' => '123456'
                                            ],
                            'phone' => [
                                        'required' => true,
                                        'type' => 'integer',
                                        'minlen' => 5,
                                        'maxlen' => 7,
                                        'value' => 743456
                                        ]
                        ]
                    ]
                ];
    }

    /**
     * Test the validator for exceptions with the values in POST
     *
     * @param array $params Array of test cases
     *
     * @dataProvider paramsWithoutValue
     * @return       assertions
     */
    public function testForExceptionsPost($params)
    {
        $_POST['email'] = '';
        $_POST['password'] = '';
        $_POST['phone'] = '';
        $validException = new ValidationException();
        $this->assertInstanceOf(ValidationException::class, $validException);
        $this->expectException(ValidationException::class);
        $validator = new Validator();
        $validator->validate($params);
    }

    /**
     * Test the validator for exceptions with the values in params
     *
     * @param array $params Array of test cases
     *
     * @dataProvider provideValueException
     * @return       assertions
     */
    public function testForExceptionsParam($params)
    {
        $validException = new ValidationException();
        $this->assertInstanceOf(ValidationException::class, $validException);
        $this->expectException(ValidationException::class);
        $validator = new Validator();
        $validator->validate($params);
    }

    /**
     * Provide parameters with invalid value for exceptions
     *
     * @return true
     */
    public function provideValueException()
    {
        return [
                    [
                        [
                            'email' => [
                                            'required' => true,
                                            'type' => 'email',
                                            'minlen' => 5,
                                            'maxlen' => 20,
                                            'value' => ''
                                            ],
                            'password' => [
                                            'required' => true,
                                            'type' => 'password',
                                            'minlen' => 5,
                                            'value' => '123456'
                                            ],
                            'phone' => [
                                        'type' => 'integer',
                                        'minlen' => 5,
                                        'maxlen' => 7,
                                        'value' => 234234
                                        ]
                        ]
                    ],
                    [
                        [
                            'email' => [
                                            'required' => false,
                                            'type' => 'email',
                                            'minlen' => 5,
                                            'maxlen' => 20,
                                            'value' => 'jamie@xyz.com'
                                            ],
                            'password' => [
                                            'required' => false,
                                            'type' => 'password',
                                            'minlen' => 5,
                                            'value' => '123456'
                                            ],
                            'phone' => [
                                        'required' => true,
                                        'type' => 'integer',
                                        'minlen' => 5,
                                        'maxlen' => 7,
                                        'value' => ''
                                        ]
                        ]
                    ]
                ];
    }
}

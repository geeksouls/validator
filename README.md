# Validators
Validators is a basic package of validation library for php projects.
Package is managed in [packagist](https://packagist.org/packages/jthedev/validators)

### Installation
```
composer require "jthedev/validators"
```
### Release versions
[1.0.0](https://packagist.org/packages/jthedev/validators)

### Contribution
All kind of contributions are highly appreciated.
>Advices and suggestions are always welcome

<?php
/**
 * Validator File
 *
 * PHP Version 5.6
 *
 * @category Class
 * @package  Validators
 * @author   Jerin Kuriakose <jerintk8@gmail.com>
 * @license  MIT License
 * @link     https://packagist.org/packages/jthedev/validators
 */
namespace Jthedev\Validators;

use Jthedev\Validators\Exceptions\ValidationException;

/**
 * Validator Class
 *
 * @category Class
 * @package  Validators
 * @author   Jerin Kuriakose <jerintk8@gmail.com>
 * @license  MIT License
 * @link     https://packagist.org/packages/jthedev/validators
 */
class Validator
{
    private $_required;
    private $_type;
    private $_minlen;
    private $_maxlen;
    private $_value;

    /**
     * __construct method
     */
    public function __construct()
    {
        $this->_required = false;
        $this->_type = 'text';
        $this->_minlen = null;
        $this->_maxlen = null;
        $this->_value = null;
    }

    /**
     * Method to validate
     *
     * @param array $params An array of validation parameters
     *
     * @return true
     */
    public function validate($params)
    {
        if (!empty($params)) {
            foreach ($params as $param => $value) {
                $this->_setDefaults($value)->_check($param);
            }
            return true;
        } else {
            throw new ValidationException("Empty params, nothing to validate");
        }
    }

    /**
     * Method to set the defaults
     *
     * @param array $constraints Set the rule for each field to check
     *
     * @return $this
     */
    private function _setDefaults($constraints)
    {
        foreach ($constraints as $constraint => $value) {
            switch ($constraint) {
            case 'required':
                $this->_required = $value;
                break;

            case 'type':
                $this->_type = $value;
                break;

            case 'minlen':
                $this->_minlen = $value;
                break;

            case 'maxlen':
                $this->_maxlen = $value;
                break;

            case 'value':
                $this->_value = $value;
                break;
            }
        }
        return $this;
    }

    /**
     * Method to start checking all constraints
     *
     * @param array $param Parameter to validate
     *
     * @return true
     */
    private function _check($param)
    {
        if ($this->_required) {
            $this->_requiredField($param);
        } else if (isset($_POST[$param])) {
            $this->_value = $_POST[$param];
        }
        $this->_typeInput($param);
        if ($this->_value != null && $this->_minlen != null) {
            $this->_minlength($param);
        }
        if ($this->_value != null && $this->_maxlen != null) {
            $this->_maxlength($param);
        }
        return true;
    }

    /**
     * Method to check whether the field is set or not
     *
     * @param array $param Parameter to validate
     *
     * @return true
     */
    private function _requiredField($param)
    {
        if (isset($_POST[$param])) {
            $this->_value = $_POST[$param];
            return true;
        } else if ($this->_value != null) {
            return true;
        } else {
            throw new ValidationException($param." is required field");
        }
    }

    /**
     * Method to check the input type
     *
     * @param array $param Parameter to validate
     *
     * @return true
     */
    private function _typeInput($param)
    {
        if ($this->_type == 'text') {
            if (preg_match('/^[a-zA-Z0-9\s]+$/', $this->_value)) {
                return true;
            } else {
                throw new ValidationException($param." is not a valid string");
            }
        }
        if ($this->_type == 'email') {
            if (preg_match(
                "/^[A-Z0-9._-]+@[A-Z0-9.-]+\.[A-Z0-9.-]+$/i",
                $this->_value
            )
            ) {
                return true;
            } else {
                throw new ValidationException($param." is not valid");
            }
        }
        if ($this->_type == 'integer') {
            if (preg_match("/^[0-9]+$/", $this->_value)) {
                return true;
            } else {
                throw new ValidationException($param." is not a valid integer");
            }
        }
    }

    /**
     * Method to check the minimum length
     *
     * @param array $param Parameter to validate
     *
     * @return true
     */
    private function _minLength($param)
    {
        if (strlen($this->_value) >= $this->_minlen) {
            return true;
        } else {
            throw new ValidationException("Length of ".$param." is too short");
        }
    }

    /**
     * Method to check the maximum length
     *
     * @param array $param Parameter to validate
     *
     * @return true
     */
    private function _maxLength($param)
    {
        if (strlen($this->_value) <= $this->_maxlen) {
            return true;
        } else {
            throw new ValidationException("Length of ".$param." is too long");
        }
    }
}

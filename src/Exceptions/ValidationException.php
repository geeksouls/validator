<?php
/**
 * ValidationException File
 *
 * PHP Version 5.6
 *
 * @category Class
 * @package  Validators
 * @author   Jerin Kuriakose <jerintk8@gmail.com>
 * @license  MIT License
 * @link     https://packagist.org/packages/jthedev/validators
 */
namespace Jthedev\Validators\Exceptions;

/**
 * ValidationException Class
 *
 * PHP Version 5.6
 *
 * @category Class
 * @package  Validators
 * @author   Jerin Kuriakose <jerintk8@gmail.com>
 * @license  MIT License
 * @link     https://packagist.org/packages/jthedev/validators
 */
class ValidationException extends \Exception
{

}
